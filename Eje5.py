class Complejo():
    def __init__(self,comp,numsum,numrest,nummult,numdiv):
        self.__complejo = comp
        self.__numero_suma = numsum
        self.__numero_resta = numrest
        self.__numero_multiplicacion = nummult
        self.__numero_division = numdiv
        
    # Getters (obtener valores)
    def get_Suma(self):
        return (self.__complejo) + (self.__numero_suma)
        
    def get_Resta(self):
        return (self.__complejo) - (self.__numero_resta)

    def get_Multiplicacion(self):
        return (self.__complejo) * (self.__numero_multiplicacion)

    def get_Division(self):
        return (self.__complejo) / (self.__numero_division)

    # Setters (cambiar valores)
    def set_Suma(self,nueva_suma):
        self.__numero_suma = nueva_suma

    def set_Resta(self,nueva_resta):
        self.__numero_resta= nueva_resta
    
    def set_Multiplicacion(self,nueva_multiplicacion):
        self.__numero_multiplicacion = nueva_multiplicacion

    def set_Division(self,nueva_division):
        self.__numero_division = nueva_division

calculos1=Complejo(3j,3.5j,4.3j,2,3j)
print("Suma = ",calculos1.get_Suma())
print("Resta = ",calculos1.get_Resta())
print("Multiplicacion = ",calculos1.get_Multiplicacion())
print("Division = ",calculos1.get_Division())
calculos1.set_Suma(4.5j)
calculos1.set_Resta(8j)
calculos1.set_Multiplicacion(0.5j)
calculos1.set_Division(10j)
print("Suma = ",calculos1.get_Suma())
print("Resta = ",calculos1.get_Resta())
print("Multiplicacion = ",calculos1.get_Multiplicacion())
print("Division = ",calculos1.get_Division())

