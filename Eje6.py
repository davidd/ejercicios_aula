from datetime import date

today=str(date.today())

print(today)

class Fecha():
    def __init__(self,año,mes,dia):
        self.__año=año
        self.__mes=mes
        self.__dia=dia

    # Getters (obtener valores)
    def get_Año (self):
        return self.__año
    def get_Mes (self):
        return self.__mes
    def get_Dia (self):
        return self.__dia
    def get_Fecha(self):
        if (self.__año)+"-"+(self.__mes)+"-"+(self.__dia)==today:
            print("Fecha correcta")
        else:
            print("Fecha incorrecta")
        return ("Fecha introducida = "+(self.__año)+"-"+(self.__mes)+"-"+(self.__dia))

    # Setters (cambiar valores)
    def set_Año(self,nuevo_año):
        self.__año = nuevo_año

    def set_Mes(self,nuevo_mes):
        self.__mes= nuevo_mes
    
    def set_Dia(self,nuevo_dia):
        self.__dia= nuevo_dia

fecha1=Fecha("2020","04","03")
print(fecha1.get_Fecha())
fecha1.set_Dia("05")
fecha1.set_Año("2021")
fecha1.set_Mes("03")
print(fecha1.get_Fecha())

