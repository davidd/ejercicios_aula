

class Contador():
    def __init__(self,cont,incr,decr):
        self.__contador=cont
        self.__incremento=incr
        self.__decremento=decr

    # Getters (obtener valores)
    def get_Contador(self):
        return self.__contador

    def get_Incrementar(self):
        return self.__incremento

    def get_Decrementar(self):
        return self.__decremento
    
    def get_Calculos_finales(self):
        total = float(self.__contador) + float(self.__incremento) - float(self.__decremento)
        return total

    # Setters (cambiar valores)
    def set_Contador(self,contador):
        self.__contador = contador
    
    def set_Incrementar(self,incremento):
        self.__incremento = incremento
    
    def set_Decrementar(self,decremento):
        self.__decremento = decremento



contador1 = Contador(0,50,30)
print("Contador inicial : ",contador1.get_Contador())
print("Incremento : ",contador1.get_Incrementar())
print("Decremento : ",contador1.get_Decrementar())
print("Total : ",contador1.get_Calculos_finales())
contador1.set_Contador(100)
contador1.set_Incrementar(300)
contador1.set_Decrementar(50)
print("Segundo contador : ",contador1.get_Contador())
print("Incremento 2 : ",contador1.get_Incrementar())
print("Decremento 2 : ",contador1.get_Decrementar())
print("Total : ",contador1.get_Calculos_finales())