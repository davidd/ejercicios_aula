
class Cuenta():
    def __init__(self,prop,numcuen,saldo,ingre,retir,transfe):
        self.__propietario = prop
        self.__numerocuenta = numcuen
        self.__saldo = saldo
        self.__ingreso = ingre
        self.__retirada = retir
        self.__transferencia = transfe

     # Getters (obtener valores)
    def get_Propietario(self):
        return self.__propietario

    def get_NumeroCuenta(self):
        return self.__numerocuenta

    def get_Saldo(self):
        return self.__saldo
    
    def get_Ingreso(self):
        return self.__ingreso

    def get_Retirada(self):
        return self.__retirada

    def get_Transferencia(self):
        return self.__transferencia
    
    def get_Calculos_finales(self):
        totalcuenta = (float(self.__saldo) + float(self.__ingreso) - float(self.__retirada) - float(self.__transferencia))
        return totalcuenta

    # Setters (cambiar valores)
    def set_Ingreso(self,ingreso):
        self.__ingreso = ingreso

    def set_Retirada(self,retirada):
        self.__retirada = retirada
    
    def set_Transferencia(self,transferencia):
        self.__transferencia = transferencia

cuenta1 = Cuenta("David",925723481954,100000,500,100,300)
print("El propietario ",cuenta1.get_Propietario()," con numero de cuenta ",cuenta1.get_NumeroCuenta()," y con un saldo de ",cuenta1.get_Saldo()," hace un ingreso de ",cuenta1.get_Ingreso()," , retira ",cuenta1.get_Retirada()," y hace una transferencia a un colega de",cuenta1.get_Transferencia()," . Finalmente le queda de saldo : ",cuenta1.get_Calculos_finales())
cuenta2 = Cuenta("Irene",456437373635,50000,34,20,100)
print("El propietario ",cuenta2.get_Propietario()," con numero de cuenta ",cuenta2.get_NumeroCuenta()," y con un saldo de ",cuenta2.get_Saldo()," hace un ingreso de ",cuenta2.get_Ingreso()," , retira ",cuenta2.get_Retirada()," y hace una transferencia a un colega de",cuenta2.get_Transferencia()," . Finalmente le queda de saldo : ",cuenta2.get_Calculos_finales())    
cuenta1.set_Ingreso(5555)
cuenta1.set_Transferencia(30)
cuenta1.set_Retirada(12)
print("El propietario ",cuenta1.get_Propietario()," con numero de cuenta ",cuenta1.get_NumeroCuenta()," y con un saldo de ",cuenta1.get_Saldo()," hace un ingreso de ",cuenta1.get_Ingreso()," , retira ",cuenta1.get_Retirada()," y hace una transferencia a un colega de",cuenta1.get_Transferencia()," . Finalmente le queda de saldo : ",cuenta1.get_Calculos_finales())
cuenta2.set_Ingreso(7777)
cuenta2.set_Transferencia(9999)
cuenta2.set_Retirada(6565)
print("El propietario ",cuenta2.get_Propietario()," con numero de cuenta ",cuenta2.get_NumeroCuenta()," y con un saldo de ",cuenta2.get_Saldo()," hace un ingreso de ",cuenta2.get_Ingreso()," , retira ",cuenta2.get_Retirada()," y hace una transferencia a un colega de",cuenta2.get_Transferencia()," . Finalmente le queda de saldo : ",cuenta2.get_Calculos_finales())
   
    