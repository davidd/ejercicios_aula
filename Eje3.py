class Libro():
    def __init__(self,nomb,prest,devol):
        self.__nombre=nomb
        self.__prestamo=prest
        self.__devolucion=devol
        

     # Getters (obtener valores)
    def get_Prestamo(self):
         return ("prestamo el "+self.__prestamo)

    def get_Devolucion(self):
         return ("devolucion el "+self.__devolucion)

    def get_Dame_info(self):
        return ("El libro titulado "+self.__nombre+" fue dado como prestamo el "+self.__prestamo+" y fue devuelto el "+self.__devolucion)

    # Setters (cambiar valores)
    def set_Prestamo(self,prestamo):
        self.__prestamo = prestamo

    def set_Devolucion(self,devolucion):
        self.__devolucion = devolucion

libro1 = Libro("La Cenicienta","4 de enero","10 de enero")
#print(libro1.get_Prestamo())
#print(libro1.get_Devolucion())
print(libro1.get_Dame_info())
libro1.set_Prestamo("5 de enero")
libro1.set_Devolucion("11 de enero")
#print(libro1.get_Prestamo())
#print(libro1.get_Devolucion())
print(libro1.get_Dame_info())

libro2 = Libro("El Principito","13 de noviembre","20 de noviembre")
#print(libro2.get_Prestamo())
#print(libro2.get_Devolucion())
print(libro2.get_Dame_info())
libro2.set_Prestamo("13 de octubre")
libro2.set_Devolucion("20 de octubre")
#print(libro2.get_Prestamo())
#print(libro2.get_Devolucion())
print(libro2.get_Dame_info())
